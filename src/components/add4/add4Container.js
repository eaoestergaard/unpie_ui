import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/add4/ActionAdd4';
import ValueSlider from './ValueSlider';
import CustomizedBtn from '../ui/CustomizedBtn';
import {ThousandFormatter} from '../ui/Formatter';
import intl from 'react-intl-universal';
import { styles } from '../../styles';
import CustomizedTable from '../ui/CustomizedTable';
import {CustomizedLineChart} from '../ui/CustomizedLineChart';

export const ValueContainer = (props) => {
  return (
    <div>
      <div className="col-sm-3">
        <ValueSlider
          updateSlider={props.actions.updateSlider}
          slider={props.add4.slider} />

        <CustomizedBtn
          updateGraph={props.actions.updateGraph}
          btnText={intl.get("RECALCULATE")}
          slider={props.add4.slider}
        />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('ADD4_TITLE')}
        </h3>
        <h4 className="text-center"  style={styles.bigPaddingTop}>
        {intl.get("MAXIMUM_ADMISSIBLE_REAL_PERIODIC_SPENDING")} <mark>{ThousandFormatter(props.add4.maxConsumption)}</mark>
        </h4>
        <CustomizedLineChart 
        data={props.add4.Scenarios} 
        xAxisLabel="N"/>
        <br />
        <br />
        <CustomizedTable
          List1={props.add4.RuinTime}
          List2={props.add4.RuinCount}
          Label1={intl.get("TIME_AFTER_RETIREMENT_YEARS")}
          Label2={intl.get("NUMBER_OF_RUINS_AFTER_T_YEARS")}
        />
      </div>
    </div>
  );
};

ValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  add4: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {add4: state.add4};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValueContainer);
