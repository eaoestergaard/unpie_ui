import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/add3/ActionAdd3';
import ValueSlider from './ValueSlider';
import CustomizedBtn from '../ui/CustomizedBtn';
import intl from 'react-intl-universal';
import CustomizedTable from '../ui/CustomizedTable';
import { styles } from '../../styles';
import {CustomizedLineChart} from '../ui/CustomizedLineChart';

export const ValueContainer = (props) => {
  return (
    <div>
      <div className="col-sm-3">

        <ValueSlider
          updateSlider={props.actions.updateSlider}
          slider={props.add3.slider} />

        <CustomizedBtn
          updateGraph={props.actions.updateGraph}
          btnText={intl.get("RECALCULATE")}
          slider={props.add3.slider}
        />

      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('ADD3_TITLE')}
        </h3>
        <h4 className="text-center" style={styles.bigPaddingTop}>
          {intl.get("DEVELOPMENT_OF_WEALTH_WHEN_SPENDING_PERIODICALLY")}
        </h4>
        <CustomizedLineChart 
        data={props.add3.Scenarios} 
        xAxisLabel="N"        />
        <br />
        <br />
        <CustomizedTable
          List1={props.add3.RuinTime}
          List2={props.add3.RuinCount}
          Label1={intl.get("TIME_AFTER_RETIREMENT_YEARS")}
          Label2={intl.get("NUMBER_OF_RUINS_AFTER_T_YEARS")}
        />
      </div>
    </div>
  );
};

ValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  add3: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { add3: state.add3 };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValueContainer);
