import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case2/ActionCase2';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartGroupTick';
import intl from 'react-intl-universal';
import {color} from '../../color';
export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('SINGLE_SAVINGS_PAYMENT_MADE_NOW'),
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('FUTURE_VALUE_NOMINAL'),
      type: 'square',
      color: color.secondary
    }, {
      value: intl.get('FUTURE_VALUE_REAL'),
      type: 'square',
      color: color.tertiary
    }
  ];
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case2UpdateSlider}
          updateGraph={props.actions.case2UpdateGraph}
          slider={props.case2.slider}/>
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE2_TITLE')}
        </h3>
        <CustomizedBarChart
          data={props.case2.barChart}
          legend={legend}
          legendLayout={"horizontal"}
          legendVerticalAlign={"bottom"}
          legendAlign={"center"}
          barSize={170}
          xaxisTextDx={15}
          />
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case2: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case2: state.case2};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
