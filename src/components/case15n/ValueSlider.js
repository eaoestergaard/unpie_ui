import React from 'react';
import PropTypes from 'prop-types';
import CustomizedSlider from '../ui/CustomizedSlider';
import CustomizedSliderSmallDigits from '../ui/CustomizedSliderSmallDigits';
import intl from 'react-intl-universal';
import {styles} from '../../styles';

class FutureValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate(){
    this.props.updateGraph(this.props.slider);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateGraph, updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PERIODIC_SPENDING_IN_RETIREMENT')}
            name = "pmt"
            min = {0}
            max = {200000}
            step = {1000}
            value = {slider.pmt}
          />

          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('WEALTH_AT_RETIREMENT')}
            name = "wealth"
            min = {0}
            max = {1000000}
            step = {1000}
            value = {slider.wealth}
          />
        </div>
        <div style={styles.sliderBox}>

          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('EXPECTED_REAL_RETURN')}
            name = "rate"
            min = {0}
            max = {0.2}
            step = {0.005}
            value = {slider.rate}
          />

          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('STANDARD_DEVIATION_OF_ANNUALIZED_REAL_LOG_RETURNS')}
            name = "vol"
            min = {0}
            max = {0.25}
            step = {0.005}
            value = {slider.vol}
          />
      </div>
      <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PENSION_AGE')}
            name = "pensionAge"
            min = {30}
            max = {100}
            step = {1}
            value = {slider.pensionAge}
          />
          <CustomizedSliderSmallDigits
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('ACCIDENTAL_DEATH_RATE')}
            name = "accidentalDeathRate"
            min = {0}
            max = {0.02}
            step = {0.001}
            value = {slider.accidentalDeathRate}
          />
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('MODAL_DEATH_RATE')}
            name = "modalDeathRate"
            min = {70}
            max = {90}
            step = {0.1}
            value = {slider.modalDeathRate}
          />
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('DISPERSION_DEATH_RATE')}
            name = "dispersionDeathRate"
            min = {1}
            max = {20}
            step = {0.1}
            value = {slider.dispersionDeathRate}
          />
        </div>
</div>
    );
  }
}



FutureValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  updateGraph: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default FutureValueSlider;
