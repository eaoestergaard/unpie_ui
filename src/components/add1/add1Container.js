import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/add1/ActionAdd1';
import ValueSlider from './ValueSlider';
import intl from 'react-intl-universal';
import CustomizedBtn from '../ui/CustomizedBtn';
import CustomizedTable from '../ui/CustomizedTable';
import {CustomizedLineChart} from '../ui/CustomizedLineChart';

export const ValueContainer = (props) => {
  return (
    <div>
      <div className="col-sm-3">
        <ValueSlider
          updateSlider={props.actions.updateSlider}
          slider={props.add1.slider} />

        <CustomizedBtn
          updateGraph={props.actions.updateGraph}
          btnText={intl.get("RECALCULATE")}
          slider={props.add1.slider}
        />
      </div>

      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('ADD1_TITLE')}
        </h3>
        <CustomizedLineChart 
        data={props.add1.Scenarios} 
        xAxisLabel="T"        />
        <br />
        <br />
        <CustomizedTable
          List1={props.add1.FutureValueSorted}
          List2={props.add1.LifeLongSorted}
          Label1={intl.get("FUTURE_VALUE_OF_SAVINGS")}
          Label2={intl.get("LIFELONG_PENSION")}
        />
      </div>
    </div>
  );
};

ValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  add1: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { add1: state.add1 };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValueContainer);
