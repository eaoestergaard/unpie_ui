import React from 'react';
import PropTypes from 'prop-types';
import CustomizedSlider from '../ui/CustomizedSliderNoUpdate';
import {styles} from '../../styles';
import intl from 'react-intl-universal';

class ValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PERIODIC_REAL_SAVING_PAYMENT')}
            name = "pv"
            min = {0}
            max = {36000}
            step = {100}
            value = {slider.pv}
          />

          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('TIME_HORIZON_YEARS_TO_RETIRE')}
            name = "nper"
            min = {1}
            max = {50}
            step = {1}
            value = {slider.nper}
          />
        </div>
        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('EXPECTED_REAL_RETURN')}
            name = "mu"
            min = {0}
            max = {0.15}
            step = {0.005}
            value = {slider.mu}
          />

          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('VOLATILITY_OF_EXPECTED_REAL_RETURN')}
            name = "vol"
            min = {0}
            max = {0.25}
            step = {0.005}
            value = {slider.vol}
          />

          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('CONVERSION_RATE')}
            name = "rate"
            min = {0.04}
            max = {0.07}
            step = {0.005}
            value = {slider.rate}
          />
        </div>
        </div>

    );
  }
}



ValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default ValueSlider;
