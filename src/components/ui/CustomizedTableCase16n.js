
import React from 'react';
import PropTypes from 'prop-types';
import {styles} from '../../styles';
//import Radium from "radium";

class CustomizedTable extends React.Component {
    render() {
      const header = this.props.Header.map((str, index)=>
      <td style={styles.table3.td} key={index}>{str}</td>
    );

    const list1 = this.props.List1.map((number, index)=>
      <td style={styles.table3.td} key={index}>{(number)}</td>
    );

    const list2 = this.props.List2.map((number, index)=>
      <td style={styles.table3.td} key={index}>{(number)}</td>
    );
    if (this.props.List1 && this.props.List1.length){
    return (
      <table style={styles.table3}>
        <tbody>
        <tr style={styles.table3.tr} key={1}>
          <th style={styles.table3.tr}>{}</th>
          {header}
        </tr>
        <tr style={styles.table3.tr} key={2}>
          <th style={styles.table3.th1}>{this.props.Label1}:</th>
          {list1}
        </tr>
        <tr style={styles.table3.tr} key={3}>
          <th style={styles.table3.th2}>{this.props.Label2}:</th>
          {list2}
        </tr>
        </tbody>
      </table>
    );
  }
  return(<div/>);
}
  
}

//CustomizedTable = Radium(CustomizedTable);

CustomizedTable.propTypes = {
  Header: PropTypes.array.isRequired,
  Label1: PropTypes.string.isRequired,
  Label2: PropTypes.string.isRequired,
  List1: PropTypes.array.isRequired,
  List2: PropTypes.array.isRequired
};

export default CustomizedTable;
