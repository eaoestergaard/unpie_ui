import React from 'react';
import PropTypes from 'prop-types';
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Bar,
  BarChart,
  Cell,
  Legend,
  Label,
  ResponsiveContainer
} from 'recharts';

import {CustomizedLabel} from './CustomizedLabel';
import {ThousandFormatter} from './Formatter';
// import {CustomizedTick} from './CustomizedTick';

export function CustomizedBarChart(props) {
  const {data,legend,text_dx, legendLayout, legendAlign, legendVerticalAlign, barSize, xaxisTextDx} = props;
  return (
    <ResponsiveContainer width={'100%'} height={360}>
    <BarChart data={data} margin={{top: 60,right: 30,left: 20,bottom: 7}}>
    <CartesianGrid vertical={false} stroke="#ebf3f0"/>
      <XAxis dataKey="text" fontFamily="sans-serif" dy={10}  >
      <Label value="T" offset={-5} dx={xaxisTextDx} position="insideTopRight" />
      </XAxis>
      <YAxis tickFormatter={ThousandFormatter} />
        <Legend payload={legend} layout={legendLayout} align={legendAlign} verticalAlign={legendVerticalAlign} wrapperStyle={style}/>
      <Bar dataKey="value" barSize={barSize} label={<CustomizedLabel dx={text_dx} />}>
        {data.map((entry, index) => (<Cell key={index} fill={legend[index].color}/>))}
      </Bar>
    </BarChart>
    </ResponsiveContainer>
  );
}
const style = {
      paddingTop: '10px',
      marginLeft: '20px',
      lineHeight: '24px',
    };

CustomizedBarChart.propTypes = {
  data: PropTypes.array.isRequired,
  legend: PropTypes.array,
  legendLayout: PropTypes.string,
  legendAlign: PropTypes.string,
  legendVerticalAlign: PropTypes.string,
  text_dx: PropTypes.number,
  xaxisTextDx: PropTypes.number,
  barSize:PropTypes.number,
};

CustomizedBarChart.defaultProps = {
  legendLayout: "vertical",
  legendAlign: "right",
  legendVerticalAlign: "middle"
};
