import React from 'react';
import PropTypes from 'prop-types';
import {styles} from '../../styles';
//import Radium from "radium";

class CustomizedBtn extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    this.props.updateGraph(this.props.slider);
  }

  render() {
    return (
      <div style={styles.sliderBoxBtn}>
            <button onClick={this.handleUpdate} style={styles.button}>
              {this.props.btnText}
            </button>
      </div>
    );
  }
}

//CustomizedBtn = Radium(CustomizedBtn);

CustomizedBtn.propTypes = {
  updateGraph: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
  btnText: PropTypes.string.isRequired
};

export default CustomizedBtn;
