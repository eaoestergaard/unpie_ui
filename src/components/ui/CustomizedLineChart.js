import React from 'react';
import PropTypes from 'prop-types';
import { XAxis, YAxis, CartesianGrid, Line, Label, ResponsiveContainer, LineChart, Tooltip} from 'recharts';
import { color } from '../../color';
import {ThousandFormatter} from './Formatter';

export function CustomizedLineChart(props) {
  const {data,xAxisLabel} = props;

  return (
    <ResponsiveContainer width={'100%'} height={360}>
          <LineChart data={data}
            margin={{ top: 60, right: 30, left: 20, bottom: 7 }}>
            <XAxis dataKey="T">
              <Label value={xAxisLabel} offset={-5} dx={15} position="insideTopRight" />
            </XAxis>
            <YAxis tickFormatter={ThousandFormatter} />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip formatter={ThousandFormatter} labelFormatter={(name) => xAxisLabel+'='+name} itemSorter={(a, b) => (a.value > b.value) ? -1 : 1}/>
            <Line type="linear" dataKey="1" stroke={color.primary} />
            <Line type="linear" dataKey="2" stroke={color.secondary} />
            <Line type="linear" dataKey="3" stroke={color.tertiary} />
            <Line type="linear" dataKey="4" stroke={color.quaternary} />
            <Line type="linear" dataKey="5" stroke={color.quinary} />
            <Line type="linear" dataKey="6" stroke={color.senary} />
            <Line type="linear" dataKey="7" stroke={color.septenary} />
            <Line type="linear" dataKey="8" stroke={color.octonary} />
            <Line type="linear" dataKey="9" stroke={color.nonary} />
            <Line type="linear" dataKey="10" stroke={color.denary} />
          </LineChart>
        </ResponsiveContainer>
  );
}

CustomizedLineChart.propTypes = {
  data: PropTypes.array.isRequired,
  xAxisLabel: PropTypes.string,
};

CustomizedLineChart.defaultProps = {
  xAxisLabel: "T"
};

