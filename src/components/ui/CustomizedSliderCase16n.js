import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'material-ui/Slider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {color} from '../../color';
import {ThousandFormatter} from './Formatter';

class CustomizedSlider extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    this.props.updateGraph(this.props.slider);  
  }

  handleChange(name, event, value) {
    const temp = {...this.props.slider};
    this.props.updateSlider(name, value, temp);
    }

  render() {
    const muiTheme = getMuiTheme({
      slider: {
        trackColor: color.secondary,
        rippleColor: color.secondary,
        selectionColor: color.secondary,
        trackColorSelected: color.secondary
      }
    });
    const {
      title,
      name,
      min,
      max,
      step,
      value
    } = this.props;
    return (
      <div>
        <div className="row">
        <label>
          {title}
        </label>
        </div>
        <div className="row">
          <div className="col-sm-9">
            <MuiThemeProvider muiTheme={muiTheme}>
              <Slider min={min} max={max} step={step} value={value} onChange={this.handleChange.bind(this, name)} onKeyUp={this.handleUpdate}  onDragStop={this.handleUpdate}/>
            </MuiThemeProvider>
          </div>
        {   name==="rate"
          ||name==="inflation"
          ||name==="mu"
          ||name==="vol"
          ||name==="prop"
          ||name==="prob"?
          <div className="col-sm-2" style={ele}>{Math.round(value * 10000) / 100}%</div>:<div className="col-sm-2" style={ele}>{ThousandFormatter(value)}</div>}
        </div>
      </div>

    );
  }
}

const ele = {
  height: "18px",
  marginTop: "22px",
  marginBottom: "22px"
};

CustomizedSlider.propTypes = {
  updateGraph: PropTypes.func,
  updateSlider: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  ele: PropTypes.object
};

export default CustomizedSlider;
