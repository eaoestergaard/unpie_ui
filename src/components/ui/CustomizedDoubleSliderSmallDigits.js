import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'material-ui/Slider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {color} from '../../color';
import {ThousandFormatter2} from './Formatter';

class CustomizedDoubleSliderSmallDigits extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    this.props.updateGraph(this.props.slider);  
  }


  handleChange(name, event, value) {
    const temp = {...this.props.slider};
    this.props.updateSlider(name, value, temp);
    }

    render() {
      const muiTheme1 = getMuiTheme({
        slider: {
          trackColor: color.secondary,
          rippleColor: color.primary,
          selectionColor: color.primary,
          trackColorSelected: color.secondary
        }
      });
      const muiTheme2 = getMuiTheme({
        slider: {
          trackColor: color.secondary,
          rippleColor: color.tertiary,
          selectionColor: color.tertiary,
          trackColorSelected: color.secondary
        }
      });
    const {
      title,
      name1,
      name2,
      min,
      max,
      step,
      value1,
      value2
    } = this.props;
    return (
      <div>
        <div className="row" style={ele3}>
        <label>
          {title}
        </label>
        </div>
          <div className="col-sm-9"style={ele2}>
            <MuiThemeProvider muiTheme={muiTheme1}>
              <Slider min={min} max={max} step={step} value={value1} onChange={this.handleChange.bind(this, name1)} onKeyUp={this.handleUpdate}  onDragStop={this.handleUpdate}/>
            </MuiThemeProvider>
          </div>
        {   name1==="rate"
          ||name1==="inflation"
          ||name1==="mu"
          ||name1==="vol"
          ||name1==="prop"
          ||name1==="prob"?
          <div className="col-sm-2" style={ele1}>{Math.round(value1 * 10000) / 100}%</div>:<div className="col-sm-2" style={ele1}>{ThousandFormatter2(value1)}</div>}
          <div className="col-sm-9" style={ele4}>
            <MuiThemeProvider muiTheme={muiTheme2}>
            <Slider min={min} max={max} step={step} value={value2} onChange={this.handleChange.bind(this, name2)} onKeyUp={this.handleUpdate}  onDragStop={this.handleUpdate}/>
            </MuiThemeProvider>
          </div>
        {   name2==="rate"
          ||name2==="inflation"
          ||name2==="mu"
          ||name2==="vol"
          ||name2==="prop"
          ||name2==="prob"?
          <div className="col-sm-2" style={ele1}>{Math.round(value2 * 10000) / 100}%</div>:<div className="col-sm-2" style={ele1}>{ThousandFormatter2(value2)} </div>}
        </div>
    );
  }
}
const ele1 = {
  height: "10px",
  marginTop: "20px",
  marginBottom: "5px"
};

const ele2 = {
  height: "0px",
  marginTop: "0px",
  marginBottom: "5px"
};

const ele3 = {
};

const ele4 = {
  height: "0px",
  marginTop: "0px",
  marginBottom: "75px"
};

CustomizedDoubleSliderSmallDigits.propTypes = {
  updateGraph: PropTypes.func.isRequired,
  updateSlider: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  name1: PropTypes.string.isRequired,
  name2: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  value1: PropTypes.number.isRequired,
  value2: PropTypes.number.isRequired,
  ele: PropTypes.object
};

export default CustomizedDoubleSliderSmallDigits;