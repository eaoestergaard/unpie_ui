import React from 'react';
import PropTypes from 'prop-types';
import { XAxis, YAxis, CartesianGrid, Area , Label, ResponsiveContainer, AreaChart , Tooltip} from 'recharts';
import { color } from '../../color';
import {ThousandFormatter, ThousandFormatter2} from './Formatter';

export function CustomizedLineChartCase16n(props) {
  return (
    <ResponsiveContainer width={'100%'} height={360}>
          <AreaChart  data={props.data}
            margin={{ top: 60, right: 30, left: 20, bottom: 7 }}>
            <XAxis dataKey="1">
              <Label value="Age (years)" offset={20} dx={+50} position="insideTopRight"/>
            </XAxis>
            <YAxis tickFormatter={ThousandFormatter} >
              <Label value="Probability" offset={-25} dx={-3} position="insideTopRight" />
            </YAxis>
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip formatter={ThousandFormatter2} />
            <Area type="monotone" dataKey="2" stroke={color.primary}  fill = {color.primary} name={"Scenario 1"}/>
            <Area type="monotone" dataKey="3" stroke={color.tertiary} fill = {color.tertiary} name={"Scenario 2"}/>
          </AreaChart > 
        </ResponsiveContainer>
  );
}

CustomizedLineChartCase16n.propTypes = {
  data: PropTypes.array.isRequired,
};

