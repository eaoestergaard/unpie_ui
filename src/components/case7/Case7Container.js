import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case7/ActionCase7';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartInf';
import {generateRepeatBarData, array2BarData} from '../../utils/dataHelper';
import {sortBy} from 'lodash';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('PERIODIC_SPENDING'), 
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('PERIODIC_SPENDING_INCREASING_TO_ACCOUNT_FOR_INFLATION'), 
      type: 'square',
      color: color.secondary
    }, {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_NOMINAL_PERIODIC_SPENDING'),
      type: 'square',
      color: color.tertiary
    }, {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_REAL_PERIODIC_SPENDING'),
      type: 'square',
      color: color.quaternary
    }
  ];

  {/* Merge data into barChart data */}
  let data = [...props.case7.barChart];
  const repeatData = generateRepeatBarData(props.case7.slider.nper + 1, props.case7.slider.pmt,legend[0]['color']);
  const repeatData2 = array2BarData(props.case7.spendingAdjInf, legend[1]['color']);
  data.push(...repeatData, ...repeatData2);
  data = sortBy(data, [function(x) {
      const numberStr = x.text;
      const number = parseInt(numberStr, 10);
      return number;
    }
  ]);

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case7UpdateSlider}
          updateGraph={props.actions.case7UpdateGraph}
          slider={props.case7.slider}/>
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE7_TITLE')}
        </h3>
        <CustomizedBarChart
          data={data}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center"
          xAxisLabel="N"
          />

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
            <h4>
            {intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_NOMINAL_PERIODIC_SPENDING')}: <mark>{ThousandFormatter(data[0].value)}</mark>
            <br />
            <br />
            {intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_REAL_PERIODIC_SPENDING')}: <mark>{ThousandFormatter(data[1].value)}</mark>
            </h4>
          </div>

      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case7: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case7: state.case7};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
