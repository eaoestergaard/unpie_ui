import React from 'react';
import PropTypes from 'prop-types';
import CustomizedSlider from '../ui/CustomizedSlider';
import CustomizedSliderSmallDigits from '../ui/CustomizedSliderSmallDigits';
import intl from 'react-intl-universal';
import {styles} from '../../styles';

class FutureValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate(){
    this.props.updateGraph(this.props.slider);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateGraph, updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
       <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PERIODIC_SPENDING_IN_RETIREMENT')}
            name = "pmt"
            min = {0}
            max = {36000}
            step = {100}
            value = {slider.pmt}
          />
        </div>

        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('INTEREST')}
            name = "rate"
            min = {0}
            max = {0.06}
            step = {0.005}
            value = {slider.rate}
          />
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('INFLATION')}
            name = "inflation"
            min = {0}
            max = {0.04}
            step = {0.005}
            value = {slider.inflation}
          />
          <CustomizedSliderSmallDigits
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('CONVERSION_RATE')}
            name = "conversionRate"
            min = {0.020}
            max = {0.150}
            step = {0.001}
            value = {slider.conversionRate}
         />
        </div>
        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PENSION_AGE')}
            name = "pensionAge"
            min = {30}
            max = {100}
            step = {1}
            value = {slider.pensionAge}
          />
          <CustomizedSliderSmallDigits
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('ACCIDENTAL_DEATH_RATE')}
            name = "accidentalDeathRate"
            min = {0}
            max = {0.02}
            step = {0.001}
            value = {slider.accidentalDeathRate}
          />
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('MODAL_DEATH_RATE')}
            name = "modalDeathRate"
            min = {70}
            max = {90}
            step = {0.1}
            value = {slider.modalDeathRate}
          />
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('DISPERSION_DEATH_RATE')}
            name = "dispersionDeathRate"
            min = {1}
            max = {20}
            step = {0.1}
            value = {slider.dispersionDeathRate}
          />
        </div>
        </div>
    );
  }
}

FutureValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  updateGraph: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default FutureValueSlider;
