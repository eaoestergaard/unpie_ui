import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/add2/ActionAdd2';
import ValueSlider from './ValueSlider';
import intl from 'react-intl-universal';
import CustomizedBtn from '../ui/CustomizedBtn';
import CustomizedTable from '../ui/CustomizedTable';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';
import {CustomizedLineChart} from '../ui/CustomizedLineChart';

export const ValueContainer = (props) => {
  return (
    <div>
      <div className="col-sm-3">
        <ValueSlider
          updateSlider={props.actions.updateSlider}
          slider={props.add2.slider} />

        <CustomizedBtn
          updateGraph={props.actions.updateGraph}
          btnText = {intl.get('RECALCULATE')}
          slider={props.add2.slider}
        />

      </div>

      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('ADD2_TITLE')}
        </h3>
        <h4 className="text-center" style={styles.bigPaddingTop}>
          {intl.get('REQUIRED_EARLY_SAVINGS_REAL')} : <mark>{ThousandFormatter(props.add2.root)} </mark>
        </h4>
        <CustomizedLineChart data={props.add2.Scenarios} />
        <br />
        <br />
        <CustomizedTable
          List1={props.add2.FutureValueSorted}
          List2={props.add2.LifeLongSorted}
          Label1={intl.get("FUTURE_VALUE_OF_SAVINGS")}
          Label2={intl.get("LIFELONG_PENSION")}
        />
      </div>
    </div>
  );
};

ValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  add2: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { add2: state.add2 };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValueContainer);
