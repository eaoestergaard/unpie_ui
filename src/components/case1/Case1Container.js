import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case1/ActionCase1';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChart';
import intl from 'react-intl-universal';
import {color} from '../../color';
export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('SINGLE_SAVINGS_PAYMENT_MADE_NOW'),
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('FUTURE_VALUE_OF_THIS_PAYMENT'),
      type: 'square',
      color: color.secondary
    }
  ];
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case1UpdateSlider}
          updateGraph={props.actions.case1UpdateGraph}
          slider={props.case1.slider}/>
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">{intl.get('CASE1_TITLE')}</h3>
        <CustomizedBarChart 
          data={props.case1.barChart} 
          legend={legend} 
          xaxisTextDx={15} 
          barSize={170}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center" />
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case1: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case1: state.case1};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
