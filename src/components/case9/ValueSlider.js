import React from 'react';
import PropTypes from 'prop-types';
import CustomizedSlider from '../ui/CustomizedSlider';
import intl from 'react-intl-universal';
import {styles} from '../../styles';

class FutureValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate(){
    this.props.updateGraph(this.props.slider);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateGraph, updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PERIODIC_REAL_SPENDING')}
            name = "pmt"
            min = {0}
            max = {36000}
            step = {100}
            value = {slider.pmt}
          />


            <CustomizedSlider
              updateGraph = {updateGraph}
              updateSlider= {updateSlider}
              slider = {slider}
              title = {intl.get('HORIZON_SAVINGS')}
              name = "nperSavings"
              min = {1}
              max = {50}
              step = {1}
              value = {slider.nperSavings}
            />

          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('HORIZON_WITHDRAWS')}
            name = "nperWithdrawals"
            min = {1}
            max = {50}
            step = {1}
            value = {slider.nperWithdrawals}
          />

        </div>
        <div style={styles.sliderBox}>

        <CustomizedSlider
          updateGraph = {updateGraph}
          updateSlider= {updateSlider}
          slider = {slider}
          title = {intl.get('INTEREST')}
          name = "rate"
          min = {0}
          max = {0.06}
          step = {0.005}
          value = {slider.rate}
        />

        <CustomizedSlider
          updateGraph = {updateGraph}
          updateSlider= {updateSlider}
          slider = {slider}
          title = {intl.get('INFLATION')}
          name = "inflation"
          min = {0}
          max = {0.04}
          step = {0.005}
          value = {slider.inflation}
        />
        </div>
      </div>

    );
  }
}



FutureValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  updateGraph: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default FutureValueSlider;
