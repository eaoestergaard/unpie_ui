import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case9/ActionCase9';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChartNoLegend} from '../ui/CustomizedBarChartNoLegend';
import intl from 'react-intl-universal';
import {updateObjectInArray} from '../../reducers/ReducerHelpers';
import {color} from '../../color';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {
  const item = [
    {
      index: 0,
      item: {
        "text": intl.get('YEARLY_REAL_SAVINGS_WHILE_WORKING'),
        "color": color.primary
      }
    }, {
      index: 1,
      item: {
        "text": intl.get('YEARLY_REAL_SPENDING_DURING_RETIREMENT'),
        "color": color.secondary
      }
    }
  ];

  let barChart = updateObjectInArray(props.case9.barChart, item[0]);
  barChart = updateObjectInArray(barChart, item[1]);

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case9UpdateSlider}
          updateGraph={props.actions.case9UpdateGraph}
          slider={props.case9.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE9_TITLE')}
        </h3>
        <CustomizedBarChartNoLegend data={barChart} />

        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
            <h4 style={styles.mediumLineHeight}>
            {intl.get("CASE9_RESULT_TXT")}<mark>{ThousandFormatter(props.case9.res)}</mark>
            </h4>
          </div>

      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case9: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case9: state.case9};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
