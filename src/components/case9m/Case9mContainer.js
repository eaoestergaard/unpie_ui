import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case9m/ActionCase9m';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChartNoLegend} from '../ui/CustomizedBarChartNoLegend';
import intl from 'react-intl-universal';
import {updateObjectInArray} from '../../reducers/ReducerHelpers';
import {color} from '../../color';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {

  const item = [
    {
      index: 0,
      item: {
        "text": intl.get('YEARLY_REAL_SAVINGS_WHILE_WORKING'),
        "color": color.primary
      }
    }, {
      index: 1,
      item: {
        "text": intl.get('YEARLY_REAL_SPENDING_DURING_RETIREMENT'),
        "color": color.secondary
      }
    }
  ];

  let barchart = updateObjectInArray(props.case9m.barChart, item[0]);
  barchart = updateObjectInArray(barchart, item[1]);
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case9mUpdateSlider}
          updateGraph={props.actions.case9mUpdateGraph}
          slider={props.case9m.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE9m_TITLE')}
        </h3>
        <CustomizedBarChartNoLegend data={barchart}/>
        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4 style={styles.mediumLineHeight}>
          {intl.get('CASE9_RESULT_TXT')} <mark>{ThousandFormatter(props.case9m.res[2])}</mark>
            <br />
            <br />
            {intl.get('EXPECTED_REMAINING_LIFETIME')}: <mark>{props.case9m.res[0]}</mark>
            <br />
            <br />
            {intl.get('FAIR_CONVERSION_RATE')}: <mark>{props.case9m.res[1]}</mark>
            </h4>
          </div>
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case9m: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case9m: state.case9m};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
