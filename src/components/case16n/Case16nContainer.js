import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case16n/ActionCase16n';
import FutureValueSlider from './ValueSlider';
import intl from 'react-intl-universal';
import CustomizedTable from '../ui/CustomizedTableCase16n';
import {CustomizedLineChartCase16n} from '../ui/CustomizedLineChartCase16n';

export const ValueContainer = (props) => {

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case16nUpdateSlider}
          slider={props.case16n.slider}
          updateGraph={props.actions.case16nUpdateGraph}
           />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE16n_TITLE')}
        </h3>
        <CustomizedLineChartCase16n data={props.case16n.remainingLifetimeDensities1} />
        <br />
        <br />
        <CustomizedTable
List1={[props.case16n.expectedRemainingLifetime1, props.case16n.medianRemainingLifetime1,props.case16n.conditionalSurvivalProbabilitiesForAnotherTYears1]}
List2={[props.case16n.expectedRemainingLifetime2, props.case16n.medianRemainingLifetime2, props.case16n.conditionalSurvivalProbabilitiesForAnotherTYears2]}
        Header={[intl.get("EXPECTED_REMAINING_LIFETIMES"),intl.get("MEDIAN_REMAINING_LIFETIMES"),intl.get("CONDITIONAL_SURVIVAL_PROBABILITIES_FOR_ANOTHER_T_YEARS")]}
        Label1={intl.get("SCENARIO_1")}
        Label2={intl.get("SCENARIO_2")}
        />
      </div>
    </div>
  );
};

ValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case16n: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case16n: state.case16n};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ValueContainer);
