import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case7mUpdateSlider(name, value, oldSlider) {
  
  const slider = Object.assign({}, oldSlider,
     {
       [name]: value
      });
  return {
    type: types.CASE7m_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
  return {
      type: types.CASE7m_FETCH_DATA_SUCCESS,
      items,
      slider
  };
}

export function case7mUpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case7m?x=${slider.pensionAge}&lambda=${slider.accidentalDeathRate}&m=${slider.modalDeathRate}&b=${slider.dispersionDeathRate}&r=${slider.rate}&inflation=${slider.inflation}&pmt=${slider.pmt}&convRate=${slider.conversionRate}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(url,func,additionParams);
}