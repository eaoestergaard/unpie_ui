import {fectAndDispact} from '../ActionHelper';
import * as types from '../../constants/actionTypes';


export function case8UpdateSlider(name,value,oldSlider){

  const slider = Object.assign({},oldSlider,
    {
      [name]:value
    });
  return{
    type: types.CASE8_UPDATE_SLIDER,
    slider
  };
}

export function case8FetchDataSuccess(items,slider) {
    return {
        type: types.CASE8_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function case8UpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case8?rate=${slider.rate}&inflation=${slider.inflation}&nperSavings=${slider.nperSavings}&nperWithdrawals=${slider.nperWithdrawals}&pmt=-${slider.pmt}`;
  const func = case8FetchDataSuccess;
  const addParams = [slider];
  return fectAndDispact(url,func,addParams);

}
