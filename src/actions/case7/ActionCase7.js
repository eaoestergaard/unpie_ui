import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case7UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE7_UPDATE_SLIDER,
    slider
  };
}

export function case7SpendingRequiredNotAdjFetchDataSuccess(items) {
  return {
      type: types.CASE7_SPENDING_REQUIRED_NOTADJINFLATION_FETCH_DATA_SUCCESS,
      items
  };
}

export function case7SpendingRequiredAdjFetchDataSuccess(items) {
    return {
        type: types.CASE7_SPENDING_REQUIRED_ADJINFLATION_FETCH_DATA_SUCCESS,
        items
    };
}

export function case7SpendingAdjFetchDataSuccess(items) {
    return {
        type: types.CASE7_SPENDING_ADJINFLATION_FETCH_DATA_SUCCESS,
        items
    };
}

export function case7UpdateGraph(slider) {
  const pv = `https://api.unpie.eu/pv.annuity?rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const wrapper = `https://api.unpie.eu/wrapper.case7?rate=${slider.rate}&inflation=${slider.inflation}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const fv = `https://api.unpie.eu/fv.single?rate=${slider.inflation}&nper=${slider.nper}&pv=-${slider.pmt}`;

  const funcPv = case7SpendingRequiredNotAdjFetchDataSuccess;
  const funcWrapper = case7SpendingRequiredAdjFetchDataSuccess;
  const funcFv = case7SpendingAdjFetchDataSuccess;

  return (dispatch) => {
    dispatch(fectAndDispact(pv,funcPv));
    dispatch(fectAndDispact(wrapper,funcWrapper));
    dispatch(fectAndDispact(fv,funcFv));
  };
}
