import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case1UpdateSlider(name,value,oldSlider){
  const slider = Object.assign({},oldSlider,{[name]:value});
  return{
    type: types.CASE1_UPDATE_SLIDER,
    slider
  };
}

export function case1FetchDataSuccess(items,slider) {
    return {
        type: types.CASE1_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function case1UpdateGraph(slider) {
  const fv = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pv=-${slider.pv}`;
  const func = case1FetchDataSuccess;
  const additionParams = [slider];

  return (dispatch)=>{
    dispatch(fectAndDispact(fv,func,additionParams));
  };
}