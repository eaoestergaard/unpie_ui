import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case5UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE5_UPDATE_SLIDER,
    slider
  };
}

export function case5FvNotAdjFetchDataSuccess(items){
  return {
      type: types.CASE5_FV_NOTADJINFLATION_FETCH_DATA_SUCCESS,
      items
  };
}

export function case5FvAdjFetchDataSuccess(items) {
    return {
        type: types.CASE5_FV_ADJINFLATION_FETCH_DATA_SUCCESS,
        items
    };
}

export function case5PmtWithInterestFetchDataSuccess(items) {
    return {
        type: types.CASE5_PMT_WITHINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case5UpdateGraph(slider) {

  const wrapper5 =  `https://api.unpie.eu/wrapper.case5?rate=${slider.rate}&inflation=${slider.inflation}&nper=${slider.nper}&pv=-${slider.pmt}`;
  const fv = `https://api.unpie.eu/fv.annuity?rate=${slider.rate}&inflation=${slider.inflation}&nper=${slider.nper}&pmt=-${slider.pmt}&pmtinfladj=TRUE`;
  const pmtAdj =  `https://api.unpie.eu/fv.single?rate=${slider.inflation}&nper=${slider.nper}&pv=-${slider.pmt}`;

  const funcWrapper5 = case5FvNotAdjFetchDataSuccess;
  const funcAdjFetch = case5FvAdjFetchDataSuccess;
  const funcPmtAdj = case5PmtWithInterestFetchDataSuccess;

    return (dispatch) => {
      dispatch(fectAndDispact(wrapper5,funcWrapper5));
      dispatch(fectAndDispact(fv,funcAdjFetch));
      dispatch(fectAndDispact(pmtAdj,funcPmtAdj));
    };
}
