import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case2UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE2_UPDATE_SLIDER,
    slider
  };
}

export function case2AdjFetchDataSuccess(items,slider) {
    return {
        type: types.CASE2_ADJINF_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function case2NotAdjFetchDataSuccess(items,slider) {
    return {
        type: types.CASE2_NOTADJINF_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function case2UpdateGraph(slider) {
  const fvNotAdj = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pv=-${slider.pv}`;
  const fvAdj = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pv=-${slider.pv}&inflation=${slider.inflation}`;

  const funcNotAdj = case2NotAdjFetchDataSuccess;
  const funcAdj = case2AdjFetchDataSuccess;
  const additionParams = [slider];

  return (dispatch)=>{
    dispatch(fectAndDispact(fvNotAdj,funcNotAdj,additionParams));
    dispatch(fectAndDispact(fvAdj,funcAdj,additionParams));
  };
}
