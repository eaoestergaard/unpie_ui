import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case16nUpdateSlider(name, value, oldSlider) {
    const slider = Object.assign({}, oldSlider,
     {
       [name]: value
      });
  return {
    type: types.CASE16n_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
  return {
      type: types.CASE16n_FETCH_DATA_SUCCESS,
      items,
      slider
  };
}

export function case16nUpdateGraph(slider) {
  const url1 = `https://api.unpie.eu/wrapper.case16nV2?x1=${slider.pensionAge1}&lambda1=${slider.accidentalDeathRate1}&m1=${slider.modalDeathRate1}&b1=${slider.dispersionDeathRate1}&t1=${slider.remainingLifetime1}&x2=${slider.pensionAge1}&lambda2=${slider.accidentalDeathRate2}&m2=${slider.modalDeathRate2}&b2=${slider.dispersionDeathRate2}&t2=${slider.remainingLifetime2}`;

  const func1 = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(url1,func1,additionParams);
}
