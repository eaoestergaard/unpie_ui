import * as i from './items';

export function fectAndDispact(url,func,addFuncParams=[]){
  return (dispatch) => {
    dispatch(i.itemsIsLoading(true));
    fetch(url)
      .then((response) => {
          if (!response.ok) {
              throw Error(response.statusText);
          }
          dispatch(i.itemsIsLoading(false));
          return response;
      })
      .then((response) => response.json())
      .then((items) => dispatch(func.apply(this,[items,...addFuncParams])))
      .catch(() => dispatch(i.itemsHasErrored(true)));
};
}
