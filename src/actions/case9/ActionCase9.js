import {fectAndDispact} from '../ActionHelper';
import * as types from '../../constants/actionTypes';


export function case9UpdateSlider(name,value,oldSlider){

  const slider = Object.assign({},oldSlider,
    {
      [name]:value
    });
  return{
    type: types.CASE9_UPDATE_SLIDER,
    slider
  };
}

export function case9FetchDataSuccess(items,slider) {
    return {
        type: types.CASE9_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function case9UpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case9?rate=${slider.rate}&inflation=${slider.inflation}&nperSavings=${slider.nperSavings}&nperWithdrawals=${slider.nperWithdrawals}&pmt=-${slider.pmt}`;
  const func = case9FetchDataSuccess;
  const addParams = [slider];
  return fectAndDispact(url,func,addParams);
}
