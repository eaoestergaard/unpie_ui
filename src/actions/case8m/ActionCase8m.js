import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case8mUpdateSlider(name, value, oldSlider) {
  
  const slider = Object.assign({}, oldSlider,
     {
       [name]: value
      });
  return {
    type: types.CASE8m_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
  return {
      type: types.CASE8m_FETCH_DATA_SUCCESS,
      items,
      slider
  };
}

export function case8mUpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case8m?x=${slider.pensionAge}&lambda=${slider.accidentalDeathRate}&m=${slider.modalDeathRate}&b=${slider.dispersionDeathRate}&t=${slider.nperSavings}&r=${slider.rate}&inflation=${slider.inflation}&pmt=-${slider.pmt}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(url,func,additionParams);
}