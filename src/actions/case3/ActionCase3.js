import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case3UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE3_UPDATE_SLIDER,
    slider
  };
}

export function case3WithoutInterestFetchDataSuccess(items) {
    return {
        type: types.CASE3_WITHOUTINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case3WithInterestFetchDataSuccess(items) {
    return {
        type: types.CASE3_WITHINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case3UpdateGraph(slider) {
  const fvWithInterest = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const fvWithOutInterest = `https://api.unpie.eu/fv?nper=${slider.nper}&pmt=-${slider.pmt}`;

  const funcWithInterest = case3WithInterestFetchDataSuccess;
  const funcWithOutInterst = case3WithoutInterestFetchDataSuccess;
  
  return (dispatch) => {
    dispatch(fectAndDispact(fvWithInterest,funcWithInterest));
    dispatch(fectAndDispact(fvWithOutInterest,funcWithOutInterst));

  };
}
