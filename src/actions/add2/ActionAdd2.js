import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function updateSlider(name,value,oldSlider){
  const slider = Object.assign({},oldSlider,
    {
      [name]:value
    });
  return{
    type: types.ADD2_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
    return {
        type: types.ADD2_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function updateGraph(slider) {
  const fv = `https://api.unpie.eu/wrapper.add2?nper=${slider.nper}&mu=${slider.mu}&sigma=${slider.vol}
  &convRate=${slider.rate}&nScenarios=1000&minPayouy=${slider.mp}&prob=${slider.prop}&seed=${1234}&returnScenarios=TRUE&numberOfScenariosToReturn=${10}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(fv,func,additionParams);
}
