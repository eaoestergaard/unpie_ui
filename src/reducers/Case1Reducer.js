import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case1(state = initialState.case1, action) {
  switch (action.type) {
    case types.CASE1_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
      case types.CASE1_FETCH_DATA_SUCCESS:
      {
        const barChart = [
          {
            "text": "0",
            "value": action.slider.pv
          }, {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length - 1]
          }
        ];
        return {
          ...state,
          barChart: barChart
        };
      }
    default:
      return state;
  }
}
