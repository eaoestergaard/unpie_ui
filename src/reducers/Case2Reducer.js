import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';

export function case2(state = initialState.case2, action) {
switch (action.type) {
  case types.CASE2_UPDATE_SLIDER:
    {
      const updateItem = {
        index: 0,
        item: {
          "text": "0",
          "value": action.slider.pv
        }
      };
      const barChart = updateObjectInArray(state.barChart, updateItem);
      return {
        ...state,
        barChart,
        slider: action.slider
      };
    }

  case types.CASE2_NOTADJINF_FETCH_DATA_SUCCESS:
    {
      const updateItem = [
        {
          index: 1,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length - 1],
            "update": true
          }
        }, {
          index: 2,
          item: {
            "update": false
          }
        }

      ];

      let barChart = updateObjectInArray(state.barChart, updateItem[0]);
      barChart = updateObjectInArray(barChart, updateItem[1]);

      return {
        ...state,
        barChart
      };
    }

    case types.CASE2_ADJINF_FETCH_DATA_SUCCESS:
      {
        const updateItem = {
          index: 2,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1],
            "update" : true //Ensure state changes
          }
        };
        const barChart = updateObjectInArray(state.barChart, updateItem);
        return {
          ...state,
          barChart
        };
      }
    default:
      return state;
  }
}
