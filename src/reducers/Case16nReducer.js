import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {mapArrayObjAdd4} from './ReducerHelpers';

export function case16n(state = initialState.case16n, action) {
  switch (action.type) {
    case types.CASE16n_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
      case types.CASE16n_FETCH_DATA_SUCCESS:
      {
        const remainingLifetimeDensities1 =  mapArrayObjAdd4(action.items.remaining_lifetime_densities);
        
        const expectedRemainingLifetime1 = action.items.expected_remaining_lifetimes1;
        const medianRemainingLifetime1 = action.items.median_remaining_lifetimes1;
        const conditionalSurvivalProbabilitiesForAnotherTYears1 = action.items.conditional_survival_probabilities_for_another_t_years1;

        const expectedRemainingLifetime2 = action.items.expected_remaining_lifetimes2;
        const medianRemainingLifetime2 = action.items.median_remaining_lifetimes2;
        const conditionalSurvivalProbabilitiesForAnotherTYears2= action.items.conditional_survival_probabilities_for_another_t_years2;

        return {
          ...state,
          remainingLifetimeDensities1,
          expectedRemainingLifetime1,
          medianRemainingLifetime1,
          conditionalSurvivalProbabilitiesForAnotherTYears1,
          expectedRemainingLifetime2,
          medianRemainingLifetime2,
          conditionalSurvivalProbabilitiesForAnotherTYears2,
          slider: action.slider,
        };
      }
    default:
      return state;
  }
}

