import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case9(state = initialState.case9, action) {
  switch (action.type) {
    case types.CASE9_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.CASE9_FETCH_DATA_SUCCESS:
      {
        const barChart = [
          {
            "text": "Yearly savings while working",
            "value": action.items[0]
          },
          {
            "text": "Yearly spending during retirement",
            "value": action.slider.pmt
          },
        ];

        const res = action.items[1];

        return {
          ...state,
          barChart: barChart,
          res: res,
          slider: action.slider
        };
      }
    default:
      return state;
  }
}
