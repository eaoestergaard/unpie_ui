import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {mapArrayObjAdd4} from './ReducerHelpers';

export function add4(state = initialState.add4, action) {
  switch (action.type) {
    case types.ADD4_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.ADD4_FETCH_DATA_SUCCESS:
      {
        const maxConsumption = action.items.res.root;
        const Scenarios = mapArrayObjAdd4(action.items.scenarios);
        const RuinTime = action.items.Ruin_time;
        const RuinCount = action.items.Ruin_count;
        return {
          ...state,
          Scenarios,
          RuinTime,
          RuinCount,
          slider: action.slider,
          maxConsumption
        };
      }
    default:
      return state;
  }
}
