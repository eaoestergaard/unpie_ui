import * as types from '../constants/actionTypes';

export function itemsHasErrored(state = false, action) {
  switch (action.type) {
    case types.ITEMS_HAS_ERRORED:
      return action.hasErrored;
    default:
      return state;
  }
}
export function itemsIsLoading(state = false, action) {
  switch (action.type) {
    case types.ITEMS_IS_LOADING:
      return action.isLoading;
    default:
      return state;
  }
}
