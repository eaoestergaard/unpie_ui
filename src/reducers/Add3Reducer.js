import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {mapArrayObjAdd3} from './ReducerHelpers';

export function add3(state = initialState.add3, action) {
  switch (action.type) {
    case types.ADD3_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.ADD3_FETCH_DATA_SUCCESS:
      {
        
        const Scenarios = mapArrayObjAdd3(action.items.Scenarios);
        const RuinTime = action.items.Ruin_time;
        const RuinCount = action.items.Ruin_count;
        return {
          ...state,
          Scenarios,
          RuinTime,
          RuinCount,
          slider: action.slider
        };
      }
    default:
      return state;
  }
}
