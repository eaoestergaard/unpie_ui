import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case8(state = initialState.case8, action) {
    switch (action.type) {
    case types.CASE8_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.CASE8_FETCH_DATA_SUCCESS:
      {
        const barChart = [
          {
            "text": "Yearly savings while working",
            "value": action.slider.pmt
          }, {
            "text": "Yearly spending during retirement",
            "value": action.items[0]
          }
        ];
        const res = action.items[1];
        return {
          ...state,
          barChart: barChart,
          res: res,
          slider: action.slider
        };
      }
    default:
      return state;
  }
}
