import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';

export function case3(state = initialState.case3, action) {
  switch (action.type) {
    case types.CASE3_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE3_WITHINTEREST_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 1,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart
        };
      }

    case types.CASE3_WITHOUTINTEREST_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 0,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart
        };
      }
    default:
      return state;
  }
}
